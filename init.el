;; Disable menu bar
(menu-bar-mode -1)
;; Disable tool bar
(tool-bar-mode -1)
;; Diable scroll bar
(scroll-bar-mode -1)
;; Show line number
(global-linum-mode t)
;; Show match paren
(show-paren-mode t)
;; Use customized linum-format: add a addition space after the line number
(setq linum-format
      (lambda (line)
        (propertize
	 (format (let ((w (length
			   (number-to-string (count-lines (point-min) (point-max))))))
		   (concat "%" (number-to-string w) "d ")) line)
	 'face 'linum)))
;; Not show the welcome buffer on startup
(setq inhibit-splash-screen 1)
;; Highlight current line
(global-hl-line-mode 1)
;; Replaces the selection if the selection is active
(delete-selection-mode 1)
;; Not save the backup file
(setq make-backup-files nil)

;; Initialize the package repo and install use-package
(require 'package)
(package-initialize)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("elpa" . "https://elpa.gnu.org/packages/"))
(defun install-package (name)
  (unless (package-installed-p name)
    (package-refresh-contents)
    (package-install name)))
;; Install use-package
(install-package 'use-package)
(require 'use-package)

;; Org-mode
(use-package org
  :ensure t
  :init
  (setq org-support-shift-select t)
  :config
  (global-set-key (kbd "C-c l") #'org-store-link)
  (global-set-key (kbd "C-c a") #'org-agenda)
  (global-set-key (kbd "C-c c") #'org-capture))
(use-package org-superstar
    :ensure t)
(add-hook 'org-mode-hook (lambda () (org-superstar-mode 1)))

;; Company mode is auto completion text
(use-package company
  :ensure t
  :init
  (setq company-idle-delay 0)
  (setq company-minimum-prefix-length 1)
  (setq company-dabbrev-downcase nil)
  :hook
  (after-init . global-company-mode))

;; Which-key will auto show the key map
(use-package which-key
  :ensure t
  :hook
  (after-init . which-key-mode))

;; File explorer
(use-package treemacs
  :ensure t)

;; Haskell mode
(use-package haskell-mode
  :ensure t)

(when (display-graphic-p)
  ;; Set cursor type to line
  (setq-default cursor-type 'bar)

  ;; Fonts
  (set-face-attribute 'default nil
		      :font "mononoki Nerd Font"
		      :width 'normal
		      :height 120
		      :weight 'light)

  ;; Set default window size and postion
  (progn
    (set-frame-width (selected-frame) 140)
    (set-frame-height (selected-frame) 55)
    (set-frame-position (selected-frame) 0 0))

  ;; Postion frame
  (use-package posframe
    :ensure t)
  (use-package which-key-posframe
    :ensure t
    :config
    (which-key-posframe-mode))
  
  ;; Theme
  (use-package github-theme
    :ensure t
    :config
    (load-theme 'github t))

  ;; Colorize color names in buffers
  ;; Such as: #ff0000
  (use-package rainbow-mode
    :ensure t))

